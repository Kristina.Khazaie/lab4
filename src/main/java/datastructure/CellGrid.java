package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int cols;
    private int rows;
    private CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.cols = columns;
        this.rows = rows;
        this.cellState = new CellState[rows][cols];

        for (int i= 0; i<rows; i++) {

            for (int j=0; j<cols; j++ ) {

                cellState[i][j] = initialState;

            }

        }

    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row<0 || row>=numRows() || column<0 || column>= numColumns()) {
            throw new IndexOutOfBoundsException("index" + row + "," + column + "is out of bounds");


        }

        cellState[row][column] = element;



    }

    @Override
    public CellState get(int row, int column) {
        if (row<0 || row>=numRows() || column<0 || column>= numColumns()) {
            throw new IndexOutOfBoundsException("index" + row + "," + column + "is out of bounds");


        }

        return cellState[row][column];
    }

    @Override
    public IGrid copy() {

        CellGrid cgCopy = new CellGrid(rows, cols, CellState.ALIVE);

        for (int i= 0; i<rows; i++) {

            for (int j=0; j<cols; j++ ) {

            cgCopy.cellState[i][j] = get(i, j);


            }

        }
        return cgCopy;
    }
    
    }
